Identifier: jupyter-notebooks.md (13)
- C
- R
- OpenRefine
- Omeka
- Google Drive
- Ruby
- Voyant Tools
- JavaScript
- Markdown
- Python
- Zenodo
- Twitter
- Github

Identifier: building-static-sites-with-jekyll-github-pages.md (11)
- WordPress
- Omeka
- Ruby
- Drupal
- Markdown
- Slack
- Projects
- Blogger
- Tumblr
- Twitter
- Jekyll

Identifier: creating-apis-with-python-and-flask.md (10)
- C
- R
- Firefox
- JavaScript
- Overview
- Python
- Paper
- BBEdit
- MediaWiki
- Twitter

Identifier: text-mining-with-extracted-features.md (10)
- C
- R
- HathiTrust Research Center
- Bookworm
- Markdown
- Python
- Matlab
- HathiTrust Digital Library
- Excel
- Github

Identifier: visualizing-with-bokeh.md (10)
- Annotate
- Gallery
- Annotations
- Bokeh
- Overview
- Python
- Leaflet
- CartoDB
- QGIS
- Excel

Identifier: mapping-with-python-leaflet.md (10)
- C
- Icon
- ArcGIS
- Nominatim
- Python
- Leaflet
- Google Maps
- OpenStreetMap
- GeoNames
- Github

Identifier: using-javascript-to-create-maps.md (10)
- C
- GPS Visualizer
- Zotero
- JavaScript
- Palladio
- Gephi
- Leaflet
- Google Maps
- MapBox
- Excel

Identifier: corpus-analysis-with-antconc.md (9)
- R
- OpenRefine
- AntConc
- Voyant Tools
- Beautiful Soup
- Python
- Concordance
- Processing
- KWIC

Identifier: creating-network-diagrams-from-historical-sources.md (9)
- C
- Palladio
- Python
- Gephi
- UCINET
- VennMaker
- Pajek
- nodegoat
- NodeXL

Identifier: sentiment-analysis.md (8)
- C
- R
- Natural Language Toolkit
- Python
- Projects
- Processing
- Twitter
- Github

Identifier: sonification.md (8)
- C
- R
- Pandoc
- Ruby
- Markdown
- Python
- Excel
- Github

Identifier: beginners-guide-to-twitter-data.md (8)
- C
- Cytoscape
- Overview
- Palladio
- Gephi
- Excel
- Twitter
- SNAP

Identifier: geoparsing-text-with-edinburgh.md (8)
- C
- Firefox
- GapVis
- Google Maps
- QGIS
- Excel
- GeoNames
- Twitter

Identifier: analyzing-documents-with-tfidf.md (8)
- C
- Bokeh
- Overview
- Python
- TF-IDF
- Paper
- Excel
- Topic modeling

Identifier: googlemaps-googleearth.md (7)
- Gallery
- ArcGIS
- Keyhole
- Google Maps
- QGIS
- Excel
- Twitter

Identifier: getting-started-with-github-desktop.md (7)
- Pandoc
- Google Drive
- Dropbox
- Markdown
- Python
- Projects
- Jekyll

Identifier: temporal-network-analysis-with-r.md (7)
- R
- Edge
- NetworkX
- Python
- Gephi
- Processing
- RStudio

Identifier: getting-started-with-mysql-using-r.md (7)
- MySQL
- C
- R
- Python
- Processing
- RStudio
- SQL Server

Identifier: introduction-to-stylometry-with-python.md (7)
- C
- R
- Natural Language Toolkit
- Papers
- Zotero
- stylo
- Python

Identifier: topic-modeling-and-mallet.md (7)
- Things
- C
- Mallet
- Voyant Tools
- Overview
- Excel
- Topic modeling

Identifier: sustainable-authorship-in-plain-text-using-pandoc-and-markdown.md (6)
- Google Docs
- WordPress
- Pandoc
- Zotero
- Markdown
- Jekyll

Identifier: introduction-to-populating-a-website-with-api-data.md (6)
- C
- Firefox
- Skype
- JavaScript
- GeoNames
- Chrome

Identifier: geocoding-qgis.md (6)
- ArcGIS
- Google Maps
- Open Attribute
- OpenStreetMap
- QGIS
- Excel

Identifier: exploring-and-analyzing-network-data-with-python.md (6)
- C
- R
- NetworkX
- Palladio
- Python
- Gephi

Identifier: introduction-to-ffmpeg.md (6)
- Projects
- Audacity
- Excel
- Twitter
- Chrome
- plot.ly

Identifier: installing-omeka.md (6)
- MySQL
- C
- R
- WordPress
- Omeka
- Omeka.net

Identifier: gravity-model.md (6)
- C
- R
- Papers
- RStudio
- Excel
- Orange

Identifier: json-and-jq.md (6)
- C
- R
- JavaScript
- Python
- Excel
- Twitter

Identifier: qgis-layers.md (6)
- ArcGIS
- OpenLayers
- Python
- Open Attribute
- QGIS
- GDAL

Identifier: graph-databases-and-SPARQL.md (5)
- Overview
- Palladio
- Excel
- GeoNames
- plot.ly

Identifier: fetch-and-parse-data-with-openrefine.md (5)
- OpenRefine
- Freebase
- Python
- Processing
- Excel

Identifier: intro-to-twitterbots.md (5)
- Python
- Slack
- Projects
- Twitter
- Github

Identifier: transforming-xml-with-xsl.md (5)
- C
- Firefox
- Excel
- Chrome
- Github

Identifier: preserving-your-research-data.md (4)
- WordPress
- Markdown
- Projects
- Excel

Identifier: extracting-illustrated-pages.md (4)
- C
- Tesseract
- Overview
- Python

Identifier: cleaning-data-with-openrefine.md (4)
- OpenRefine
- Overview
- Freebase
- Excel

Identifier: dealing-with-big-data-and-network-analysis-using-neo4j.md (4)
- Papers
- Python
- Projects
- Excel

Identifier: intro-to-powershell.md (4)
- C
- Mallet
- Pandoc
- Python

Identifier: keywords-in-context-using-n-grams.md (4)
- Firefox
- Zotero
- Python
- KWIC

Identifier: cleaning-ocrd-text-with-regular-expressions.md (4)
- C
- R
- Python
- Excel

Identifier: OCR-with-Tesseract-and-ScanTailor.md (4)
- C
- Tesseract
- Zotero
- Tesseract OCR

Identifier: generating-an-ordered-data-set-from-an-OCR-text-file.md (4)
- C
- JavaScript
- Python
- Perl

Identifier: data-mining-the-internet-archive.md (4)
- C
- Python
- Google Maps
- Wordle

Identifier: correspondence-analysis-in-R.md (4)
- C
- R
- Zenodo
- Processing

Identifier: geospatial-data-analysis.md (4)
- C
- R
- Google Maps
- plot.ly

Identifier: extracting-keywords.md (4)
- OpenRefine
- Python
- QGIS
- Excel

Identifier: working-with-web-pages.md (3)
- Firefox
- Python
- Transcript

Identifier: basic-text-processing-in-r.md (3)
- R
- Processing
- RStudio

Identifier: creating-and-viewing-html-files-with-python.md (3)
- Firefox
- Zotero
- Python

Identifier: getting-started-with-markdown.md (3)
- Pandoc
- Markdown
- Perl

Identifier: counting-frequencies-from-zotero-items.md (3)
- Zotero
- Google Books
- Python

Identifier: georeferencing-qgis.md (3)
- Google Maps
- QGIS
- GDAL

Identifier: naive-bayesian.md (3)
- Beautiful Soup
- Python
- TF-IDF

Identifier: output-keywords-in-context-in-html-file.md (3)
- Firefox
- Python
- KWIC

Identifier: transliterating.md (3)
- R
- Beautiful Soup
- Python

Identifier: introduction-and-installation.md (3)
- Dropbox
- Beautiful Soup
- Python

Identifier: intro-to-beautiful-soup.md (3)
- Overview
- Beautiful Soup
- Python

Identifier: r-basics-with-tabular-data.md (3)
- C
- R
- Excel

Identifier: data_wrangling_and_management_in_R.md (3)
- R
- Python
- RStudio

Identifier: ocr-tutorial.md (3)
- C
- JavaScript
- Python

Identifier: output-data-as-html-file.md (3)
- Firefox
- Zotero
- Python

Identifier: research-data-with-unix.md (2)
- Excel
- Pattern

Identifier: creating-new-items-in-zotero.md (2)
- Zotero
- Python

Identifier: intro-to-augmented-reality-with-unity.md (2)
- C
- Projects

Identifier: intro-to-the-zotero-api.md (2)
- Zotero
- Python

Identifier: mac-installation.md (2)
- Beautiful Soup
- Python

Identifier: editing-audio-with-audacity.md (2)
- Soundflower
- Audacity

Identifier: windows-installation.md (2)
- C
- Python

Identifier: applied-archival-downloading-with-wget.md (2)
- Papers
- Python

Identifier: intro-to-bash.md (2)
- Pandoc
- Markdown

Identifier: vector-layers-qgis.md (2)
- Google Maps
- QGIS

Identifier: automated-downloading-with-wget.md (2)
- C
- Python

Identifier: installing-python-modules-pip.md (2)
- C
- Python

Identifier: up-and-running-with-omeka.md (2)
- Omeka
- Omeka.net

Identifier: working-with-text-files.md (2)
- C
- Python

Identifier: creating-an-omeka-exhibit.md (2)
- R
- Omeka

Identifier: understanding-regular-expressions.md (2)
- Ruby
- Python

Identifier: intro-to-linked-data.md (1)
- GeoNames

Identifier: manipulating-strings-in-python.md (1)
- Python

Identifier: creating-mobile-augmented-reality-experiences-in-unity.md (1)
- C

Identifier: from-html-to-list-of-words-2.md (1)
- Python

Identifier: viewing-html-files.md (1)
- Firefox

Identifier: from-html-to-list-of-words-1.md (1)
- Python

Identifier: code-reuse-and-modularity.md (1)
- Python

Identifier: counting-frequencies.md (1)
- Python

Identifier: normalizing-data.md (1)
- Python

Identifier: downloading-multiple-records-using-query-strings.md (1)
- Python

Identifier: linux-installation.md (1)
- Python
