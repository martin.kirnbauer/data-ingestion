#!/bin/bash
echo "now I am running"
# get arguments from DPU
. $1

#iterate through all files
for filename in $tools_dir/*.json; do
    # append the  attributes file onto the tools file
    JSON=$(head --bytes -1 ${filename} && echo ",\"attributes\":" && cat $attributes_dir/$(basename "$filename") && echo ",\"tags\":" && cat $tags_dir/$(basename "$filename") && echo ",\"suggested\":" && cat $suggested_dir/$(basename "$filename") && echo "}")    
    # output to the output folder
    echo $JSON > $output_dir/$(basename "$filename")    
done
